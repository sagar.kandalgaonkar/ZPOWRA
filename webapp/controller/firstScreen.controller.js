sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/ui/model/odata/v2/ODataModel",
	'sap/ui/model/Filter'
], function (Controller, MessageBox, ODataModel, Filter) {
	"use strict";
	var riskAssess;
	var userId;
	return Controller.extend("zPowra.zPowra.controller.firstScreen", {
		onInit: function () {
			/*	var UserInfo = sap.ushell.Container.getService("UserInfo");
				var user = UserInfo.getUser();
				var userName = user.getFullName();
				userId = user.getId();
				this.getView().byId("inputPar").setValue(userName);
				this.getView().byId("inputPiit").setValue(userName); */ ///works only on fiori launchpad
			sap.ui.core.IconPool.addIcon('Height', 'customfont', 'icomoon', 'e94c');
			sap.ui.core.IconPool.addIcon('Confined', 'customfont', 'icomoon', 'e90c');
			sap.ui.core.IconPool.addIcon('Fire', 'customfont', 'icomoon', 'e91c');
			sap.ui.core.IconPool.addIcon('Asp', 'customfont', 'icomoon', 'e900');
			sap.ui.core.IconPool.addIcon('Dust', 'customfont', 'icomoon', 'e912');
			sap.ui.core.IconPool.addIcon('Temp', 'customfont', 'icomoon', 'e932');
			sap.ui.core.IconPool.addIcon('Lift', 'customfont', 'icomoon', 'e923');
			sap.ui.core.IconPool.addIcon('Subs', 'customfont', 'icomoon', 'e93e');
			sap.ui.core.IconPool.addIcon('Ec', 'customfont', 'icomoon', 'e918');
			sap.ui.core.IconPool.addIcon('Traffic', 'customfont', 'icomoon', 'e948');
			sap.ui.core.IconPool.addIcon('Noise', 'customfont', 'icomoon', 'e92d');
			sap.ui.core.IconPool.addIcon('Slips', 'customfont', 'icomoon', 'e938');
			sap.ui.core.IconPool.addIcon('Manual', 'customfont', 'icomoon', 'e928');
			this.oModel = new sap.ui.model.json.JSONModel(jQuery.sap.getModulePath("zPowra.zPowra.model",
				"/Data.json"));
			this.getView().setModel(this.oModel, "oModelData");
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1; //January is 0!
			var yyyy = today.getFullYear();
			var hours = today.getHours();
			var min = today.getMinutes();
			var sec = today.getSeconds();
			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (min < 10) {
				min = '0' + min;
			}
			if (sec < 10) {
				sec = '0' + sec;
			}
			today = mm + '/' + dd + '/' + yyyy + " " + " " + hours + ':' + min + ':' + sec;
			this.getView().byId("todayDate").setText(today);
			var self = this;
			var serviceUrl = "/questionSet";
			this.getOwnerComponent().getModel("defaultModel").read(serviceUrl, {
				//filters: newsol,
				success: function (oData) {
					self.oModel = new sap.ui.model.json.JSONModel();
					self.oModel.setData(oData);
					self.getView().setModel(self.oModel);
					var hazardsData = self.getView().getModel("oModelData").getData().Forms;
					var array = [];
					var obj = {};
					var oRootPath = jQuery.sap.getModulePath("zPowra.zPowra");
					for (var i = 0; i < oData.results.length; i++) {
						if (oData.results[i].Qtype === "00") {
							//	oData.results[i].Value = "YES,NO,NOT APPLICABLE"; /*testing*/
							var val = oData.results[i].Value;
							var options = val.split(",");
							if (options[0] === "YES") {
								oData.results[i].Yes = true;
							}
							if (options[1] === "NO") {
								oData.results[i].No = true;
							}
							if (options[2] === "NA") {
								oData.results[i].Na = true;
							}
							array.push(oData.results[i]);
							array[i].path = oRootPath;
						}
					}
					obj.Forms = array;
					self.oModel = new sap.ui.model.json.JSONModel();
					self.oModel.setData(obj);
					self.getView().setModel(self.oModel, "oModel");
					for (var j = 0; j < hazardsData.length; j++) {
						hazardsData[j].path = oRootPath;
						for (var k = 0; k < oData.results.length; k++) {
							if (hazardsData[j].Qtype === oData.results[k].Qtype) {
								hazardsData[j]["ques"] = [];
								hazardsData[j].ques.push(oData.results[k]);
								hazardsData[i].path = oRootPath;
							}
						}
					}
					self.oModelHazards = new sap.ui.model.json.JSONModel();
					self.oModelHazards.setData(hazardsData);
					self.getView().setModel(self.oModelHazards, "oModelHazards");
					self.autoPopulate(); /*for autopopulating bus dep team fields*/
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});

			/*	var oModel3 = new sap.ui.model.odata.ODataModel(
					"/sap/opu/odata/sap/ZODATA_APA_E_0279_POWRA_SRV/", true);
				oModel3.read("/ZCDS_APA_E_0279_CHARAC", null, ["$format=json"], true, function(
					data) {
					//	var length = data.results.length;
					//	var newData = data.results.slice(10, length);
					self.oModel = new sap.ui.model.json.JSONModel();
					self.oModel.setData(data);
					self.getView().setModel(self.oModel);
					var hazardsData = self.getView().getModel("oModelData").getData().Forms;
					var array = [];
					var obj = {};
					for (var i = 0; i < data.results.length; i++) {
						if (data.results[i].qtype === "00") {
							array.push(data.results[i]);
						}

					}
					obj.Forms = array;
					self.oModel = new sap.ui.model.json.JSONModel();
					self.oModel.setData(obj);
					self.getView().setModel(self.oModel, "oModel");

					for (var j = 0; j < hazardsData.length; j++) {
						for (var k = 0; k < data.results.length; k++) {
							if (hazardsData[j].qtype === data.results[k].qtype) {
								hazardsData[j]["ques"] = [];
								hazardsData[j].ques.push(data.results[k]);
							}
						}
					}
					self.oModelHazards = new sap.ui.model.json.JSONModel();
					self.oModelHazards.setData(hazardsData);
					self.getView().setModel(self.oModelHazards, "oModelHazards");
				}, function(msg) {
					sap.m.MessageToast.show("Connection not established");
				});*/
		},
		handleLocation: function (oEvent) {
			var self = this;
			var serviceUrlLocation = "/ZCDS_APA_E_0279_F4FLOC";
			this.getOwnerComponent().getModel("defaultModel").read(serviceUrlLocation, {
				//filters: newsol,
				success: function (oData) {
					self.oModelLocation = new sap.ui.model.json.JSONModel();
					self.oModelLocation.setData(oData);
					self.getView().setModel(self.oModelLocation);
					self._oDialog.setModel(self.oModelLocation); /*duplication */
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});
			this.id = oEvent.getParameter("id");
			if (!this._oDialog) {
				this._oDialog = sap.ui.xmlfragment("zPowra.zPowra.fragments.location", this);
				//	this._oDialog.setModel(this.oModelLocation);
			}
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialog);
			this._oDialog.open();
		},
		autoPopulate: function (oEvent) {
			var self = this;
			var serviceUrlPrsnInvol = "/zcds_apa_e_0279_perfullname";
			this.getOwnerComponent().getModel("defaultModel").read(serviceUrlPrsnInvol, {
				//filters: newsol,
				success: function (oData) {
					self.autoPopulate = new sap.ui.model.json.JSONModel();
					self.autoPopulate.setData(oData);
					self.getView().setModel(self.autoPopulate);
					//var value = userId; /*launchapd uncomment*/
					var value = 'KANDALGS'; /*comment*/
					var data = self.autoPopulate.getData();
					for (var i = 0; i < data.results.length; i++) {
						if (data.results[i].usrid === value) {
							var pernr = data.results[i].pernr;
							break;
						}
					}
					self.busDepTeam(pernr);
					//self._oDialogPrsnInvol.setModel(self.oModelPrsnInvol);
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});
			/*	this.idPrsn = oEvent.getParameter("id");
				if (!this._oDialogPrsnInvol) {
					this._oDialogPrsnInvol = sap.ui.xmlfragment("zPowra.zPowra.fragments.personInvolved", this);
					//	this._oDialog.setModel(this.oModelLocation);
				}
				// toggle compact style
				jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogPrsnInvol);
				this._oDialogPrsnInvol.open();*/
		},

		handlePrsnInvol: function (oEvent) {
			/*var self = this;
			var serviceUrlPrsnInvol = "/zcds_apa_e_0279_perfullname";
			this.getOwnerComponent().getModel("defaultModel").read(serviceUrlPrsnInvol, {
				//filters: newsol,
				success: function (oData) {
					self.oModelPrsnInvol = new sap.ui.model.json.JSONModel();
					self.oModelPrsnInvol.setData(oData);
					self.getView().setModel(self.oModelPrsnInvol);
					self._oDialogPrsnInvol.setModel(self.oModelPrsnInvol);
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});*/
			this.idPrsn = oEvent.getParameter("id");
			if (!this._oDialogPrsnInvol) {
				this._oDialogPrsnInvol = sap.ui.xmlfragment("zPowra.zPowra.fragments.personInvolved", this);
				this._oDialogPrsnInvol.setModel(this.autoPopulate);
			}
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogPrsnInvol);
			this._oDialogPrsnInvol.open();
		},

		handleJobNumber: function (oEvent) {
			var self = this;
			var serviceUrlPrsnInvol = "/zcds_apa_e_0279_jobnum";
			this.getOwnerComponent().getModel("defaultModel").read(serviceUrlPrsnInvol, {
				//filters: newsol,
				success: function (oData) {
					self.oModelJobNumber = new sap.ui.model.json.JSONModel();
					self.oModelJobNumber.setData(oData);
					self.getView().setModel(self.oModelJobNumber);
					self._oDialogJobNumber.setModel(self.oModelJobNumber);
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});
			this.idJob = oEvent.getParameter("id");
			if (!this._oDialogJobNumber) {
				this._oDialogJobNumber = sap.ui.xmlfragment("zPowra.zPowra.fragments.jobNumber", this);
				//	this._oDialogJobNumber.setModel(this.oModelJobNumber);
			}
			// toggle compact style
			jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getView(), this._oDialogPrsnInvol);
			this._oDialogJobNumber.open();
		},
		handleJobSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([new Filter(
				[
					new Filter("aufnr",
						sap.ui.model.FilterOperator.Contains,
						sValue),
					new Filter("ktext",
						sap.ui.model.FilterOperator.Contains,
						sValue)
				], false)]);
		},
		handleJobConfirm: function (oEvent) {
			var value = oEvent.getParameter("selectedItem").getTitle();
			this.getView().byId(this.idJob).setValue(value);
		},
		handleLocationSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			/*	var oFilter = (new sap.ui.model.Filter(new sap.ui.model.Filter("tplnr", sap.ui.model.FilterOperator.Contains, sValue),
					new sap.ui.model.Filter("pltxt", sap.ui.model.FilterOperator.Contains, sValue)), false);*/
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([new Filter(
				[
					new Filter("tplnr",
						sap.ui.model.FilterOperator.Contains,
						sValue),
					new Filter("pltxt",
						sap.ui.model.FilterOperator.Contains,
						sValue)
				], false)]);
			//	oBinding.filter([oFilter]);
			/*var sValue = oEvent.getParameter("value");
			var items = {
				path: '/ZCDS_APA_E_0279_F4FLOC',
				filters: [
					new sap.ui.model.Filter("tplnr", sap.ui.model.FilterOperator.Contains, sValue)
				],
				template: new sap.m.StandardListItem({
					title: "{tplnr}",
					description: "{pltxt}"
				})
			};
			var oList = oEvent.getSource().getAggregation("_dialog").getContent()[1];
			oList.bindItems(items);*/
		},
		handlePrsnInvolSearch: function (oEvent) {
			var sValue = oEvent.getParameter("value");
			var oFilter = new sap.ui.model.Filter("ename", sap.ui.model.FilterOperator.Contains, sValue);
			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},
		handlePrsnInvolConfirm: function (oEvent) {
			var value = oEvent.getParameter("selectedItem").getTitle();
			var data = this.autoPopulate.getData();
			for (var i = 0; i < data.results.length; i++) {
				if (data.results[i].ename === value) {
					var pernr = data.results[i].pernr;
					break;
				}
			}
			this.getView().byId(this.idPrsn).setValue(value);
			this.busDepTeam(pernr);
		},
		busDepTeam: function (pernr) {
			var self = this;
			var oModelBus = new sap.ui.model.odata.ODataModel(
				"/sap/opu/odata/sap/ZODATA_APA_E_0279_POWRA_SRV/", true);
			//	var serviceUrl2 = "/zcds_apa_e_0279_orgdata?$filter= usrid eq 'KANDALGS'"; /**uncomment this when fiori launchpad*/
			var serviceUrl2 = "/zcds_apa_e_0279_orgdata?$filter= pernr eq '" + pernr + "'"; /*comment this when fiori launchpad*/
			oModelBus.read(serviceUrl2, {
				//filters: newsol,
				success: function (oData) {
					if (oData.results.length !== 0) {
						var bu = oData.results[0].butext;
						var dep = oData.results[0].depttext;
						var team = oData.results[0].teamtext;
						self.getView().byId("inputBu").setValue(bu);
						self.getView().byId("inputDep").setValue(dep);
						self.getView().byId("inputTeam").setValue(team);
					}
				},
				error: function (oData) {
					sap.m.MessageToast.show("Connection not established");
				}
			});
		},
		handleLocationConfirm: function (oEvent) {
			var value = oEvent.getParameter("selectedItem").getTitle();
			var desc = oEvent.getParameter("selectedItem").getDescription();
			this.getView().byId(this.id).setValue(value);
			this.getView().byId("inputLocDesc").setValue(desc);
		},
		onNextPress2: function () {
			//	this.onSubmitForm(); /*testing*/
			this.navTo().to(this.createId("page4"));
		},
		navTo: function () {
			var result = this.byId("app");
			return result;
		},
		onProceedPress: function () {
			this.navTo().to(this.createId("page5"));
		},
		onNavBack: function () {
			this.navTo().to(this.createId("page3"));
		},
		onCancelPage5: function () {
			this.navTo().to(this.createId("page4"));
		},
		onAcceptPress: function (oEvent) {
			var count = 0;
			oEvent.getSource().setProperty("color", "#4682B4");
			var parent = oEvent.getSource().getParent().getParent();
			parent.getContent()[2].setVisible(false); /*for making stopcard invisible*/
			var text = oEvent.getSource().getParent().getParent().getContent()[0].getProperty("text");

			/*special case hardcoded */
			if (text === "Is a risk assessment in place for the task?") {
				oEvent.getSource().getParent().getParent().getContent()[3].setVisible(true);
				//	riskAssess = oEvent.getSource().getParent().getParent().getContent()[3].getItems()[1].getProperty("value");
			}
			/*special case*/
			var items = parent.getParent().getItems();
			for (var i = 0; i < items.length; i++) {
				var color = items[i].getContent()[1].getItems()[0].getProperty("color");
				var colorNa = items[i].getContent()[1].getItems()[2].getProperty("color");
				if (color === "#4682B4" || colorNa === "#4682B4") {
					this.getView().byId("proceed").setEnabled(false);
					count = count + 1;
				}
				if (count === items.length) {
					this.getView().byId("proceed").setEnabled(true);
				}

			}
			var index = oEvent.getParameter("id");
			var value = index.slice(30);
			//	var modelData = this.getView().getModel("undefined").getData();

			var option = oEvent.getSource().getProperty("alt");
			if (option === "accept") {
				var item = oEvent.getSource().getParent().getItems()[1]; /*for changing other options color*/
				item.setProperty("color", "#b4b4b4");
				var item2 = oEvent.getSource().getParent().getItems()[2]; /*for changing other options color*/
				item2.setProperty("color", "#b4b4b4");

				/*setting data to model for POST*/
				this.oModel.getData().Forms[value].Characteristicvalue = "YES";
			} else {
				var item3 = oEvent.getSource().getParent().getItems()[0]; /*for changing other options color*/
				item3.setProperty("color", "#b4b4b4");
				var item4 = oEvent.getSource().getParent().getItems()[1]; /*for changing other options color*/
				item4.setProperty("color", "#b4b4b4");
				oEvent.getSource().getParent().getParent().getContent()[3].setVisible(false);
				/*setting data to model for POST*/
				this.oModel.getData().Forms[value].Characteristicvalue = "NA";
			}

		},
		onRejectPress: function (oEvent) {
			oEvent.getSource().setProperty("color", "#4682B4");
			var parent = oEvent.getSource().getParent().getParent();
			parent.getContent()[2].setVisible(true);

			/*special case stopCard hide start*/
			oEvent.getSource().getParent().getParent().getContent()[3].setVisible(false);
			/*end*/
			var items = parent.getParent().getItems();
			for (var i = 0; i < items.length; i++) {
				var color = items[i].getContent()[1].getItems()[1].getProperty("color");
				if (color === "#4682B4") {
					this.getView().byId("proceed").setEnabled(false);
				}
			}
			var item = oEvent.getSource().getParent().getItems()[0];
			item.setProperty("color", "#b4b4b4");
		},
		onAcceptPressImageHazards: function (oEvent) {
			var count = 0,
				iconclicked = 0;
			oEvent.getSource().setProperty("color", "#4682B4");
			oEvent.getSource().getParent().getParent().getParent().getContent()[2].setVisible(false);
			var items = oEvent.getSource().getParent().getParent().getParent().getParent().getItems();
			for (var i = 0; i < items.length; i++) {
				var iconselected = oEvent.getSource().getParent().getParent().getParent().getParent().getItems()[i].getContent()[0].getItems()[0]
					.getProperty(
						"color");
				if (iconselected === "black") {
					var visible = items[i].getContent()[1].getVisible();
					if (visible === true) {
						iconclicked = iconclicked + 1;
					} else {
						iconclicked = 0;
					}
					var color = items[i].getContent()[1].getItems()[1].getItems()[0].getColor();

					if (color === "#4682B4") {
						this.getView().byId("submit").setEnabled(false);
						count = count + 1;
					}
					if (count === iconclicked) {
						this.getView().byId("submit").setEnabled(true);
					}
				}
			}
			var item = oEvent.getSource().getParent().getItems()[1];
			item.setProperty("color", "#b4b4b4");
		},
		onRejectPressImageHazards: function (oEvent) {
			oEvent.getSource().setProperty("color", "#4682B4");
			oEvent.getSource().getParent().getParent().getParent().getContent()[2].setVisible(true);
			var items = oEvent.getSource().getParent().getParent().getParent().getParent().getItems();
			for (var i = 0; i < items.length; i++) {
				var color = items[i].getContent()[1].getItems()[1].getItems()[1].getColor();
				if (color === "#4682B4") {
					this.getView().byId("submit").setEnabled(false);
				}
			}
			var item = oEvent.getSource().getParent().getItems()[0];
			item.setProperty("color", "#b4b4b4");
		},

		onIconPress: function (oEvent) {
			oEvent.getSource().getParent().getParent().getContent()[2].setVisible(false);
			var color = oEvent.getSource().getProperty("color");
			if (color === "#E6E6FA") {
				var index = oEvent.getSource().getProperty("alt");
				var newIndex;
				if (index < 10) {
					newIndex = index.slice(1, 2) - 1;
				} else {
					newIndex = index - 1;
				}
				var data = this.getView().getModel("oModelHazards").getData();
				if (data[newIndex].ques === undefined) {
					oEvent.getSource().setProperty("color", "black");
					oEvent.getSource().getParent().getParent().getContent()[1].setVisible(false);
				} else {
					oEvent.getSource().setProperty("color", "black");
					oEvent.getSource().getParent().getParent().getContent()[1].setVisible(true);
				}
			} else {
				oEvent.getSource().setProperty("color", "#E6E6FA");
				oEvent.getSource().getParent().getParent().getContent()[1].setVisible(false);
			}
			var items = oEvent.getSource().getParent().getParent().getParent().getItems();
			for (var i = 0; i < items.length; i++) {
				var iconselected = items[i].getContent()[0].getItems()[0].getProperty("color");
				if (iconselected === "black") {
					this.getView().byId("submit").setEnabled(true);
					break;
				} else {
					this.getView().byId("submit").setEnabled(false);
				}
			}
			/*else {
				this.getView().byId("submit").setEnabled(false);
			}*/

		},
		handleSpcRsk: function (oEvent) {
			var value = oEvent.getParameter("value");
			riskAssess = value;
		},
		/*for Attachment upload*/
		onChange: function (oEvent) {
			/*if (!this.NotiNo) {
				sap.m.MessageToast.show("Please first submit POWRA form");
			} else {*/
			var oUploadCollection = oEvent.getSource();
			this.draftFile = false;
			this.csrfToken = "";
			var oUploadCheck = "/sap/opu/odata/sap/ZODATA_APA_E_0279_POWRA_SRV/filestreamSet";
			var url = new ODataModel(oUploadCheck);
			var csrfToken = url.getSecurityToken();

			var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
				name: "x-csrf-token",
				value: csrfToken
			});
			oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
			var oView = this.getView().byId("UploadCollection");
			oView.setUploadUrl("/sap/opu/odata/sap/ZODATA_APA_E_0279_POWRA_SRV/filestreamSet");
			/*}*/
		},

		onBeforeUploadStarts: function (oEvent) {
			// Header Slug

			var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
				name: "slug",
				value: this.NotiNo + ';' + oEvent.getParameter("fileName")
			});
			oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);

		},
		onUploadComplete: function () {
			sap.m.MessageToast.show("Image Uploaded for Notification" + this.NotiNo);
			this.Flag = true;
		},
		/*attachment upload*/

		onSubmitForm: function () {
			if (this.Flag === true) {
				var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
				MessageBox.success(
					"PoWRa Form Submitted", {
						actions: [sap.m.MessageBox.Action.OK],
						styleClass: bCompact ? "sapUiSizeCompact" : "",
						onClose: function () {
							window.history.go(-1);
						}

					}
				);
			} else {
				var _self = this;
				var dialog = new sap.m.Dialog({
					title: 'Confirm',
					type: 'Message',
					content: new sap.m.Text({
						text: 'Are you sure you want to submit PoWRA Form?'
					}),
					beginButton: new sap.m.Button({
						text: 'Submit',
						press: function () {
							sap.ui.core.BusyIndicator.show();
							var date = _self.getView().byId("todayDate").getText();
							var year = date.substring(6, 10);
							var month = date.substring(0, 2);
							var day = date.substring(3, 5);
							//2018-06-26T12:12:12
							/*	var hours = date.substring(12, 14);
								var min = date.substring(15, 17);
								var sec = date.substring(18, 22);*/
							var temp = date.split(":");
							var hours = temp[0].slice(12);
							var min = temp[1];
							var sec = temp[2];
							var newDate = year + '-' + month + '-' + day + 'T' + hours + ':' + min + ':' + sec;

							var newTime = 'PT' + hours + 'H' + min + 'M' + '00' + 'S';
							var payload = {
								chrct: {
									results: []
								}
							};
							//	var data = {};
							payload.Perassignrskcode = _self.getView().byId("inputPar").getValue();
							payload.Perinvolvedesc = _self.getView().byId("inputPiit").getValue();
							payload.Notino = "";
							payload.Businessunitdesc = _self.getView().byId("inputBu").getValue();
							payload.Departmentdesc = _self.getView().byId("inputDep").getValue();
							payload.Teamdesc = _self.getView().byId("inputTeam").getValue();
							payload.Jobnodesc = _self.getView().byId("inputJob").getValue();
							payload.Conducteddate = newDate;
							payload.Conductedtime = newTime;
							payload.Task = _self.getView().byId("textTask").getValue();
							payload.Flocationcode = _self.getView().byId("inputLoc").getValue();
							payload.Locationdesc = _self.getView().byId("inputLocDesc").getValue();
							payload.Riskref = riskAssess;
							payload.Listofhazards = _self.getView().byId("hazardTextArea").getValue();
							var a = [];
							_self.modelData = _self.oModel.getData();
							for (var i = 0; i < _self.modelData.Forms.length; i++) {
								var data = {};
								data.Perassignrskcode = _self.getView().byId("inputPar").getValue();
								data.Perinvolvedesc = _self.getView().byId("inputPiit").getValue();
								data.Notino = "";
								data.Code = "";
								data.Codegrp = "";
								data.Characteristicname = _self.modelData.Forms[i].Characteristicname;
								data.Characteristicvalue = _self.modelData.Forms[i].Characteristicvalue;
								a.push(data);
								//payload.chrct.results.push(data);
							}
							payload.chrct.results = a;

							var readReqURL = "/sap/opu/odata/sap/ZODATA_APA_E_0279_POWRA_SRV/";
							this.oModelOdata2 = new sap.ui.model.odata.ODataModel(readReqURL);
							this.oModelOdata2.create("/POWRASet", payload, {
								success: function (oData, oResponse) {
									_self.NotiNo = oResponse.data.Notino;
									sap.ui.core.BusyIndicator.hide();
									_self.notificationSet();
								},
								error: function (oData, oResponse) {
									sap.ui.core.BusyIndicator.hide();
									sap.m.MessageToast.show("Data not updated");
								}
							});
							dialog.close();
						}
					}),
					endButton: new sap.m.Button({
						text: 'Cancel',
						press: function () {
							dialog.close();
						}
					}),
					afterClose: function () {
						dialog.destroy();
					}
				});
				dialog.open();
			}
		},

		notificationSet: function () {
			var _self = this;
			sap.ui.core.BusyIndicator.show();
			//	var serviceUrl2 = "/NotiCloseSet?$filter= NotiNo eq '" + _self.NotiNo + "'";
			var payload = {

			};
			payload.NotiNo = _self.NotiNo;
			_self.getOwnerComponent().getModel("defaultModel").create("/NotiCloseSet", payload, {
				success: function (oData) {
					sap.ui.core.BusyIndicator.hide();
					/*	var bCompact = !!_self.getView().$().closest(".sapUiSizeCompact").length;
						MessageBox.success(
							"PoWRa Form Submitted", {
								styleClass: bCompact ? "sapUiSizeCompact" : ""
							}
						);*/
					var bCompact = !!_self.getView().$().closest(".sapUiSizeCompact").length;
					MessageBox.confirm(
						"Do you want to Attach any Image?", {
							actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
							styleClass: bCompact ? "sapUiSizeCompact" : "",
							onClose: function (sAction) {
								if (sAction === "YES") {
									_self.getView().byId("imageUploadBlock").setVisible(true);
									_self.Flag = true;
								} else {
									_self.getView().byId("imageUploadBlock").setVisible(false);
									_self.Flag = true;
									MessageBox.success(
										"PoWRa Form Submitted", {
											actions: [sap.m.MessageBox.Action.OK],
											styleClass: bCompact ? "sapUiSizeCompact" : "",
											onClose: function () {
												window.history.go(-1);
											}
										}
									);

								}
							}
						}
					);
				},
				error: function (oData) {
					sap.ui.core.BusyIndicator.hide();
					sap.m.MessageToast.show("Connection not established");
				}
			});
		}

	});
});